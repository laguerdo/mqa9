import express from 'express';
import bodyParser from 'body-parser';
import routes from './routes/index';
// Init express
const app = express();
// Set port-nya
const port = process.env.PORT || 8000;
// Konfigurasi app ke user bodyParser
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
// Register routes ke variabel app
app.use('/', routes);
// Start server
app.listen(port, () => {
    console.log(`Server started on port ${port}`);
});
// Export
export default app;